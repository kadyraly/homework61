import React from 'react';
import './CountyData.css';

const CountryData = props => {
    if (props.country) {
        return (
            <div className="CountryData">
                <h1> {props.country.name}</h1>
                <p><strong>Capital:</strong> {props.country.capital}</p>
                <p><strong>Population:</strong> {props.country.population}</p>
                <img src={props.country.flag} width="100px" height="50px"/>
                <p><strong>Borders:</strong></p>
                 {props.borders.map(border => {
                    return <ul key={border.name}>{border.name}</ul>
                })}
            </div>
        )
    } else {
        return <h1>ничего не выбрано</h1>
    }
};

export default CountryData;