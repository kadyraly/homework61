import React from 'react';
import './CountryList.css';

const CountryList = props => {
    return (
        <div className="CountryList">
            {props.countries.map(country =>{
                return <div key={country.alpha3Code} style={{cursor: 'pointer'}} onClick={() => props.click(country.name, country.alpha3Code)}>{country.name}</div>


            })}
        </div>
    )
};

export default CountryList;