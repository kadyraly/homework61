import React, { Component } from 'react';
import axios from 'axios';

import './App.css';
import CountryList from "./components/CountryList/CountryList";
import CountryData from "./components/CountryData/CountryData";

class App extends Component {



    constructor(props) {
        super(props);
        this.state = {
            countries: [],
            borders: [],
            country: null
        };
    }



    componentDidMount() {
        const BASE_URL = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code;borders';

                axios.get(BASE_URL)
                    .then(response => {
                        this.setState({countries: response.data});

                    });

    }

    getInfo=(name, code) => {
        let countries = this.state.countries;
        let index = countries.findIndex((country) => country.name === name);
        console.log(countries[index])

        axios.get('https://restcountries.eu/rest/v2/alpha/' + code)
            .then(res => this.setState({country: res.data}));

        let borders = countries[index].borders;
        console.log(borders)
        Promise.all(borders.map(border => {
           return axios.get('https://restcountries.eu/rest/v2/alpha/' + border)
        })).then(res =>{
            console.log(res);
            const borders = res.map(r => r.data);
            this.setState({borders: borders})
        })
    };


    render() {
        return (
            <div>
                <CountryList countries={this.state.countries} click ={this.getInfo}/>
                <CountryData country={this.state.country}
                             borders={this.state.borders}   />
            </div>
        );
      }
}

export default App;
